# -*- coding: utf-8 -*-
from __future__ import unicode_literals


__all__ = [
    '__version__', '__version_info__',
    '__title__', '__summary__', '__url__',
    '__author__', '__email__', '__license__',
]

__title__ = 'bots-ediint'

__version_info__ = (3, 8, 5)
__version__ = '.'.join(map(str, __version_info__))

__summary__ = """Bots EDI Translator"""

__license__ = "GNU General Public License (GPL v3.0)"

__author__ = "The Bots developers"
__email__ = "ludovic.watteaux@gmail.com"

__url__ = "https://gitlab.com/bots-ediint/bots"
