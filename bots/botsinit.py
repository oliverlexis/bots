from __future__ import unicode_literals, print_function

import codecs
import encodings
import logging
import logging.handlers
import os
import shutil
import sys

import django
import django.conf

# bots-modules
from . import botsglobal
from . import botslib
from . import node

if sys.version_info[0] > 2:
    basestring = unicode = str
    import configparser
else:
    import ConfigParser as configparser


LOG_FORMAT = '%(asctime)s %(levelname)-9s [%(name)s] %(message)s'
LOG_DT_FORMAT = '%Y.%m.%d %H:%M:%S'
LOG_CONSOLE_FORMAT = LOG_FORMAT


class BotsConfig(configparser.RawConfigParser, object):
    """As ConfigParser, but with defaults."""

    def get(self, section, option, default='', **kwargs):
        if self.has_option(section, option):
            result = super(BotsConfig, self).get(section, option)
            return result or default
        if default == '':
            raise botslib.BotsError(
                'No entry "%(option)s" in section "%(section)s" in "bots.ini".', {
                    'option': option, 'section': section,
                }
            )
        return default

    def getint(self, section, option, default, **kwargs):
        if self.has_option(section, option):
            return configparser.RawConfigParser.getint(self, section, option)
        return default

    def getboolean(self, section, option, default, **kwargs):
        if self.has_option(section, option):
            return configparser.RawConfigParser.getboolean(self, section, option)
        return default


def initbotsenv(**kwargs):
    """
    Initialise bots user env (config, botssys, usersys) with bots directories bots.[config,...]
    if configdir doesn't exist.
    By default bots env is in:
        ~/.bots/env/$USER

    You can specify a different bots env with env var $BOTSENV:
        ~/.bots/env/$BOTSENV

    :param botsenv:
        - With only botsenv specified, botsenv_path = ~/.bots/env/botsenv/

    :param configdir:
        This will install bots config dir in specified path.
        Note if no botsenv_path arg is specified, botsenv_path will be in ~/.bots/env/?botsenv

    :param botsenv_path:

    ex:
    >>> initbotsenv(botsenv_path='/path/to/botsenv')
    # Will Create:
        /path/to/botsenv/config
        /path/to/botsenv/botsys
        /path/to/botsenv/usersys
    """
    botsenv = kwargs.get('botsenv') or os.environ.get('BOTSENV') \
        or os.environ.get('USER') or 'default'
    botsenv_path = kwargs.get('botsenv_path') \
        or os.path.join(os.path.expanduser('~'), '.bots', 'env', botsenv)
    configdir = kwargs.get('configdir') or os.path.join(botsenv_path, 'config')

    if os.sep in configdir and not os.path.isdir(configdir):
        bots_path = os.path.dirname(__file__)
        # Setup new bots env
        print('Installation of bots env:', botsenv_path, file=sys.stderr)
        if kwargs.get('interactive'):
            confirm = input('Confirm creation of bots env: %s (y/n) ? ' % botsenv_path)
            if confirm.lower() not in ['y', 'yes']:
                print('Bots env creation canceled.', file=sys.stderr)
                return
        try:
            # config
            print('Installing bots config:', configdir, file=sys.stderr)
            os.makedirs(configdir)
            # Copy bots.ini and settings.py from bots/install/ in configdir/
            for configfile in ['bots.ini', 'settings.py']:
                shutil.copy(os.path.join(bots_path, 'install', configfile), configdir)
                open(os.path.join(configdir, '__init__.py'), 'w').write('')
            # botssys
            botssys = os.path.join(botsenv_path, 'botssys')
            if not os.path.exists(botssys):
                print('Installing botssys:', botssys, file=sys.stderr)
                sqlitedb = os.path.join(botssys, 'sqlitedb')
                os.makedirs(sqlitedb)
                shutil.copy(os.path.join(bots_path, 'install', 'botsdb'), sqlitedb)
            # usersys
            usersys = os.path.join(botsenv_path, 'usersys')
            if not os.path.exists(usersys):
                print('Installing bots usersys:', usersys, file=sys.stderr)
                shutil.copytree(os.path.join(bots_path, 'usersys'), usersys)

            # bots.ini: Set botssys and usersys path
            botsini = open(os.path.join(configdir, 'bots.ini'), 'r').read()
            open(os.path.join(configdir, 'bots.ini'), 'w').write(botsini.replace(
                'botssys = botssys', 'botssys = %s' % botssys).replace(
                'usersys = usersys', 'usersys = %s' % usersys))

        except Exception as exc:
            raise botslib.PanicError(
                'Error while installing bots config "%s" : %s' % (configdir, exc))

    return configdir


def generalinit(configdir=None):
    """Load bots config from config dir."""
    if not configdir:
        if os.environ.get('BOTS_CONFIG_DIR'):
            # config dir set from env var $BOTS_CONFIG_DIR
            configdir = os.path.normpath(os.environ.get('BOTS_CONFIG_DIR'))
            # if not os.path.isdir(configdir):
            #     raise botslib.PanicError(_("Bots config dir doesn't exist: %s") % configdir)
        else:
            # config dir set in user space ~/.bots/env/$BOTSENV/config
            botsenv = os.environ.get('BOTSENV') or os.environ.get('USER') or 'default'
            botsenv_path = os.path.join(os.path.expanduser('~'), '.bots', 'env', botsenv)
            configdir = os.path.join(botsenv_path, 'config')
            initbotsenv(**locals())

    botsglobal.configdir = configdir = configdir.rstrip(os.path.sep)

    ##########################################################################
    # Configdir: settings.py & bots.ini ######################################
    # Configdir MUST be importable. So configdir is relative to PYTHONPATH.
    # Try several options for this import.
    try:
        # first check if is configdir outside bots-directory: import configdir.settings.py
        os.environ['DJANGO_SETTINGS_MODULE'] = importnameforsettings = os.path.normpath(
            os.path.join(configdir, 'settings')
        ).replace(os.sep, '.')
        settings = botslib.botsbaseimport(importnameforsettings)
    except ImportError:
        # normal: configdir is in bots directory: import bots.configdir.settings.py
        try:
            os.environ['DJANGO_SETTINGS_MODULE'] = importnameforsettings = os.path.normpath(
                os.path.join('bots', configdir, 'settings')
            ).replace(os.sep, '.')
            settings = botslib.botsbaseimport(importnameforsettings)
        except ImportError:
            # set pythonpath to config directory first
            # check if configdir exists.
            if not os.path.exists(configdir):
                raise botslib.PanicError(
                    'In initilisation: path to configuration does not exists: "%(configdir)s".', {
                        'configdir': configdir,
                    }
                )
            addtopythonpath = os.path.abspath(os.path.dirname(configdir))
            moduletoimport = os.path.basename(configdir)
            if addtopythonpath not in sys.path:
                # print('Adding python path:', addtopythonpath)
                sys.path.append(addtopythonpath)
            importnameforsettings = '.'.join((moduletoimport, 'settings'))
            os.environ['DJANGO_SETTINGS_MODULE'] = importnameforsettings
            # print('importnameforsettings:', importnameforsettings)
            settings = botslib.botsbaseimport(importnameforsettings)
            if configdir not in settings.__file__:
                raise botslib.PanicError(
                    'In initilisation: settings file imported "%(settings_file)s" '
                    'not in BOTS_CONFIG_DIR "%(configdir)s".'
                    'Fixe PYTHON_PATH or rename BOTS_CONFIG_DIR folder %(moduletoimport)s.', {
                        'settings_file': settings.__file__,
                        'configdir': configdir,
                        'moduletoimport': moduletoimport,
                    }
                )

    # settings is imported, so now we know where to find settings.py: importnameforsettings
    # note: the imported settings.py itself is NOT used, this is doen via django.conf.settings
    configdirectory = os.path.abspath(os.path.dirname(settings.__file__))
    # Read configuration-file bots.ini.
    botsglobal.ini = BotsConfig()
    botsglobal.ini.read(os.path.join(configdirectory, 'bots.ini'))
    # 'directories','botspath': absolute path for bots directory
    botsglobal.ini.set('directories', 'botspath', os.path.abspath(os.path.dirname(__file__)))
    # 'directories','config': absolute path for config directory
    botsglobal.ini.set('directories', 'config', configdirectory)
    # set config as originally received; used in starting engine via bots-monitor
    botsglobal.ini.set('directories', 'config_org', configdir)

    ###########################################################################
    # Usersys #################################################################
    # usersys MUST be importable. So usersys is relative to PYTHONPATH.
    # Try several options for this import.
    usersys = os.path.normpath(botsglobal.ini.get('directories', 'usersys', 'usersys'))
    try:
        # usersys outside bots-directory: import usersys
        importnameforusersys = usersys.replace(os.sep, '.')
        importedusersys = botslib.botsbaseimport(importnameforusersys)
    except ImportError:
        try:
            # usersys is in bots directory: import bots.usersys
            importnameforusersys = os.path.join('bots', usersys).replace(os.sep, '.')
            importedusersys = botslib.botsbaseimport(importnameforusersys)
        except ImportError:
            # set pythonpath to usersys directory first
            if not os.path.exists(usersys):  # check if configdir exists.
                raise botslib.PanicError(
                    'In initilisation: path to configuration does not exists: "%(usersys)s".', {
                        'usersys': usersys,
                    }
                )
            # Usersys directory is absolute path
            addtopythonpath = os.path.abspath(os.path.dirname(usersys))
            importnameforusersys = os.path.basename(usersys)
            if addtopythonpath not in sys.path:
                sys.path.append(addtopythonpath)
            importedusersys = botslib.botsbaseimport(importnameforusersys)

    # 'directories', 'usersysabs': absolute path for config usersysabs
    # Find pathname usersys using imported usersys
    botsglobal.ini.set('directories', 'usersysabs', importedusersys.__path__[0])
    # botsglobal.usersysimportpath: used for imports from usersys
    botsglobal.usersysimportpath = importnameforusersys
    botsglobal.ini.set(
        'directories', 'templatehtml', botslib.join(
            botsglobal.ini.get('directories', 'usersysabs'),
            'grammars/templatehtml/templates'
        )
    )

    ############################################################################
    # Botssys ##################################################################
    # 'directories','botssys': absolute path for config botssys
    botssys = botsglobal.ini.get('directories', 'botssys', 'botssys')
    # store original botssys setting
    botsglobal.ini.set('directories', 'botssys_org', botssys)
    # use absolute path
    botsglobal.ini.set('directories', 'botssys', botslib.join(botssys))
    botsglobal.ini.set('directories', 'data', botslib.join(botssys, 'data'))
    botsglobal.ini.set('directories', 'logging', botslib.join(botssys, 'logging'))
    botsglobal.ini.set('directories', 'users', botslib.join(botssys, '.users'))
    # dirmonitor trigger
    botsglobal.ini.set('dirmonitor', 'trigger', botslib.join(botssys, '.dirmonitor.trigger'))
    botsglobal.ini.set('settings', 'log_when', botsglobal.ini.get('settings', 'log_when', 'report'))
    ############################################################################
    # other inits ##############################################################
    # values in bots.ini are also used in setting up cherrypy
    if botsglobal.ini.get('webserver', 'environment', 'development') != 'development':
        # during production: if errors occurs in writing to log: ignore error.
        # (leads to a missing log line, better than error;-).
        logging.raiseExceptions = 0

    botslib.dirshouldbethere(botsglobal.ini.get('directories', 'data'))
    botslib.dirshouldbethere(botsglobal.ini.get('directories', 'logging'))
    # initialise bots charsets
    initbotscharsets()
    node.Node.checklevel = botsglobal.ini.getint('settings', 'get_checklevel', 1)
    botslib.settimeout(botsglobal.ini.getint('settings', 'globaltimeout', 10))

    ############################################################################
    # Init django ##############################################################
    if not django.conf.settings.configured:
        django.setup()

    # settings are accessed using botsglobal
    botsglobal.settings = django.conf.settings

    # Djediint add-on
    if 'djediint' in botsglobal.settings.INSTALLED_APPS:
        import djediint.bots


# **********************************************************************************
# *** bots specific handling of character-sets (eg UNOA charset) *******************
def initbotscharsets():
    """set up right charset handling for specific charsets (UNOA, UNOB, UNOC, etc)."""
    # tell python how to search a codec defined by bots. Bots searches for this in usersys/charset
    codecs.register(codec_search_function)
    # syntax has parameters checkcharsetin or checkcharsetout. These can have value 'botsreplace'
    # eg: 'checkcharsetin':'botsreplace',  #strict, ignore or botsreplace
    # in case of errors: the 'wrong' character is replaced with char as set in bots.ini.
    # Default value in bots.ini is ' ' (space)
    botsglobal.botsreplacechar = unicode(botsglobal.ini.get('settings', 'botsreplacechar', ' '))
    # need to register the handler for botsreplacechar
    codecs.register_error('botsreplace', botsreplacechar_handler)
    # set aliases for the charsets in bots.ini
    for key, value in botsglobal.ini.items('charsets'):
        encodings.aliases.aliases[key] = value


def codec_search_function(encoding):
    try:
        module, filename = botslib.botsimport('charsets', encoding)
    except botslib.BotsImportError:
        # charsetscript not there; other errors like syntax errors are not catched
        return None
    if hasattr(module, 'getregentry'):
        return module.getregentry()
    return None


def botsreplacechar_handler(info):
    """
    replaces an char outside a charset by a user defined char.
    Useful eg for fixed records: recordlength does not change.
    """
    return (botsglobal.botsreplacechar, info.start + 1)


# *** end of bots specific handling of character-sets ******************************
# **********************************************************************************


def connect():
    """connect to database for non-django modules eg engine"""
    db_engine = botsglobal.settings.DATABASES['default']['ENGINE']
    if db_engine == 'django.db.backends.sqlite3':
        # sqlite has some more fiddling; in separate file.
        # Mainly because of some other method of parameter passing.
        if not os.path.isfile(botsglobal.settings.DATABASES['default']['NAME']):
            raise botslib.PanicError('Could not find database file for SQLite')
        from . import botssqlite

        botsglobal.db = botssqlite.connect(
            database=botsglobal.settings.DATABASES['default']['NAME']
        )
    elif db_engine == 'django.db.backends.mysql':
        import MySQLdb
        from MySQLdb import cursors

        botsglobal.db = MySQLdb.connect(
            host=botsglobal.settings.DATABASES['default']['HOST'],
            port=int(botsglobal.settings.DATABASES['default']['PORT']),
            db=botsglobal.settings.DATABASES['default']['NAME'],
            user=botsglobal.settings.DATABASES['default']['USER'],
            passwd=botsglobal.settings.DATABASES['default']['PASSWORD'],
            cursorclass=cursors.DictCursor,
            **botsglobal.settings.DATABASES['default']['OPTIONS']
        )
    elif db_engine.startswith('django.db.backends.postgresql'):
        import psycopg2
        import psycopg2.extensions
        import psycopg2.extras

        psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
        botsglobal.db = psycopg2.connect(
            host=botsglobal.settings.DATABASES['default']['HOST'],
            port=botsglobal.settings.DATABASES['default']['PORT'],
            database=botsglobal.settings.DATABASES['default']['NAME'],
            user=botsglobal.settings.DATABASES['default']['USER'],
            password=botsglobal.settings.DATABASES['default']['PASSWORD'],
            connection_factory=psycopg2.extras.DictConnection,
        )
        botsglobal.db.set_client_encoding('UNICODE')
    else:
        raise botslib.PanicError('Unknown database engine "%s".' % db_engine)


# *******************************************************************
# *** init logging **************************************************
# *******************************************************************
STARTINFO = 28
LOG_LEVELS = {
    'DEBUG': logging.DEBUG,
    'STDOUT': 11,  # coms & jobqueue-server job stdout
    'STDERR': 12,  # coms & jobqueue-server job stderr
    'INFO': logging.INFO,
    'COM': 25,
    'DONE': 26,
    'START': 27,
    'STARTINFO': STARTINFO,
    'WARNING': logging.WARNING,
    'ERROR': logging.ERROR,
    'CRITICAL': logging.CRITICAL,
}
logging.addLevelName(LOG_LEVELS['STARTINFO'], 'STARTINFO')
logging.addLevelName(LOG_LEVELS['STDOUT'], 'STDOUT')
logging.addLevelName(LOG_LEVELS['STDERR'], 'STDERR')
logging.addLevelName(LOG_LEVELS['START'], 'START')
logging.addLevelName(LOG_LEVELS['DONE'], 'DONE')


def initenginelogging(logname):
    """initialise engine logging: create engine logger."""
    logger = logging.getLogger(logname)
    proc_name = logname.replace('%s.' % __package__, '')
    logdir = os.path.join(botsglobal.ini.get('directories', 'logging'), proc_name)
    botslib.dirshouldbethere(logdir)
    log_when = botsglobal.ini.get('settings', 'log_when', None)
    if log_when == 'daily':
        handler = logging.handlers.TimedRotatingFileHandler(
            os.path.join(logdir, proc_name + '.log'),
            when='midnight',
            backupCount=botsglobal.ini.getint('settings', 'log_file_number', 30),
        )
    else:
        handler = logging.handlers.RotatingFileHandler(
            os.path.join(logdir, proc_name + '.log'),
            backupCount=botsglobal.ini.getint('settings', 'log_file_number', 10),
        )
        if log_when is None:
            # each run a new log file is used; old one is rotated
            handler.doRollover()
    fileformat = logging.Formatter(LOG_FORMAT, LOG_DT_FORMAT)
    handler.setFormatter(fileformat)
    handler.setLevel(botsglobal.ini.get('settings', 'log_file_level', 'INFO'))
    logger.addHandler(handler)

    # initialise file logging: logger for trace of mapping;
    # tried to use filters but got this not to work ...
    botsglobal.logmap = logging.getLogger('engine.map')
    if not botsglobal.ini.getboolean('settings', 'mappingdebug', False):
        botsglobal.logmap.setLevel(logging.CRITICAL)
    # logger for reading edifile. is now used only very limited (1 place); is done with 'if'
    # botsglobal.ini.getboolean('settings', 'readrecorddebug', False)

    # initialise console/screen logging
    if botsglobal.ini.getboolean('settings', 'log_console', True):
        console = logging.StreamHandler()
        consoleformat = logging.Formatter(LOG_CONSOLE_FORMAT, LOG_DT_FORMAT)
        # add formatter to console
        console.setFormatter(consoleformat)
        # Set console log level
        console.setLevel(botsglobal.ini.get('settings', 'log_console_level', 'INFO'))
        # add console to logger
        logger.addHandler(console)

    # Global Bots LOG LEVEL: bots.engine, bots.engine2
    logger.setLevel(botsglobal.ini.get('settings', 'log_level', 'INFO'))
    if not botsglobal.ini.get('settings', 'log_level', None):
        for handler in logger.handlers:
            if handler.level < logger.level:
                logger.setLevel(handler.level)
    return logger


def initserverlogging(logname):
    """initialise file logging"""
    logger = logging.getLogger(logname)
    proc_name = logname.replace('%s.' % __package__, '').replace('jobqueueserver', 'jobqueue')
    logdir = os.path.join(botsglobal.ini.get('directories', 'logging'), proc_name)
    botslib.dirshouldbethere(logdir)
    handler = logging.handlers.TimedRotatingFileHandler(
        os.path.join(logdir, proc_name + '.log'),
        when='midnight',
        backupCount=botsglobal.ini.getint(
            proc_name, 'log_file_number', botsglobal.ini.getint('settings', 'log_file_number', 30)),
    )
    fileformat = logging.Formatter(LOG_FORMAT, LOG_DT_FORMAT)
    handler.setFormatter(fileformat)
    handler.setLevel(botsglobal.ini.get(proc_name, 'log_file_level', 'INFO'))
    logger.addHandler(handler)

    # initialise console/screen logging
    if botsglobal.ini.getboolean(proc_name, 'log_console', True):
        console = logging.StreamHandler()
        consoleformat = logging.Formatter(LOG_CONSOLE_FORMAT, LOG_DT_FORMAT)
        # add formatter to console
        console.setFormatter(consoleformat)
        # Set console log level
        console.setLevel(botsglobal.ini.get(proc_name, 'log_console_level', 'STARTINFO'))
        # add console to logger
        logger.addHandler(console)

    # Bots server(s) LOG LEVEL: bots.jobqueue, bots.dirmonitor, bots.webserver
    logger.setLevel(botsglobal.ini.get(proc_name, 'log_level', 'INFO'))
    if not botsglobal.ini.get(proc_name, 'log_level', None):
        for handler in logger.handlers:
            if handler.level < logger.level:
                logger.setLevel(handler.level)
    return logger
