# Generated by Django 3.2.9 on 2022-01-17 16:21

import bots.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bots', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='channel',
            name='inorout',
            field=bots.models.StripCharField(choices=[('in', 'in'), ('out', 'out')], max_length=3, verbose_name='in/out'),
        ),
    ]
