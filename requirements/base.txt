Django>=1.11
cheroot
Cherrypy
Django<2; python_version < '3.4'
Django<2.1; python_version == '3.4'
cheroot==8.6.0; python_version <= '3.4'
Cherrypy==16.0.3; python_version <= '3.4'
lxml
